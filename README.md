# Summarry

This is the repository for all Cloud Functions used by the viMeet App.

# Emulator nutzen

**Wichtig für alle Aktionen muss man sich im Ordner functions befinden**

Für die Entwicklung ist eine Emulator Suite den Cloud Functions beigefügt. Diese besteht aus
- lokaler Authentifizierung
- lokalem Firestore
- lokalen Coudfunctions
Man kann sie nutzen indem man in Flutter `useEmulator=true` setzt (im main file).

Anschließend können die lokalen Emulatoren gestartet werden mit `firebase emulators:start`. Nach jeder Änderung an den Cloud Functions kann man diese synchronisieren über `npm run build` (am besten aus anderer Konsole dann muss man die Emulatoren nicht immer neu starten)

# Deployen von Cloud Functions
- Depolyen alle Funktionen über `firebase deploy --only functions`
- Deployen einzelner Module über `firebase deploy --only functions:<modul>`

# Struktur 
Die verschiedenen Funktionen werden intern durch module gekapselt analog zu https://firebase.google.com/docs/functions/organize-functions
