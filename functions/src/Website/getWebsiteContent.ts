export function getWebsiteContent(content: FirebaseFirestore.DocumentData) {
    let updates = '';
    for (const update of content.updates) {
        updates =
            updates +
            `<div class="update_base">
            <div class="update_date">` +
            update.text_update_date +
            `</div>
            <div class="update_content">
                <h3>` +
            update.text_update_title +
            `</h3>
                <p>` +
            update.text_update_desc +
            `</p>
            </div>
        </div>`;
    }

    return (
        `<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css2?family=Titillium+Web:wght@300;600&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Fredoka+One&display=swap" rel="stylesheet">
    <link rel="shortcut icon" type="image/x-icon" href="./assets/favicon.ico">
    <script type="text/javascript" src="data/updates.js"></script>
    <script type="text/javascript" src="logic/base.js"></script>
    <meta charset="UTF-8">
    <title>viMeet</title>
    <!--<link rel="stylesheet" href="css/bootstrap.css">-->
    <link rel="stylesheet" href="style/base.css">
</head>

<body>
    <div class="header_base center_horiz">
        <div class="header content_container">
            <a href=""> <img src="./assets/img/brand/logo_wide_white.png" id="logo"></a>
            <div class="navbar">
                <a href="#features">Funktionen</a>
                <a href="#about">Über uns</a>
                <a href="#development">Updates</a>
                <a href="#contact">Kontakt</a>
            </div>
            <div class="navbar_mobile_icon" id="mobile_menu_button"></div>
        </div>
    </div>
    <div class="navbar navbar_mobile" id="mobile_menu">
        <a href="#features">Funktionen</a>
        <a href="#about">Über uns</a>
        <a href="#development">Updates</a>
        <a href="#contact">Kontakt</a>
    </div>
    <div class="center appimage_base" id="appimage">
        <div class="content_container">
            <div class="appimage_image">
                <video muted autoplay loop id="appimage_hero_video">
                    <source src="./assets/vid/vimeet_hero_video.mp4">
                </video>
            </div>
        </div>
    </div>
    <div class="hero_base center_horiz" id="hero">
        <div class="content_container hero">
            <div class="appimage_placeholder"></div>
            <div class="hero_content">
                <div class="hero_text">` +
        content.text_hero_main +
        `</div>
                <a class="anchor" id="download"></a>
                <div class="hero_dlbuttons">
                    <!--<a
                        href='https://play.google.com/store/apps/details?id=org.mozilla.firefox&pcampaignid=pcampaignidMKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1'><img
                            alt='Jetzt bei Google Play'
                            src='https://play.google.com/intl/en_us/badges/static/images/badges/de_badge_web_generic.png' /></a>
                    <a
                        href="https://apps.apple.com/us/app/firefox-private-safe-browser/id989804926?itsct=apps_box&amp;itscg=30200"><img
                            src="https://tools.applemediaservices.com/api/badges/download-on-the-app-store/black/de-DE?size=250x83&amp;releaseDate=1447286400&h=505680a4db0efa7df962ba62e39e08d4"
                            alt="Download on the App Store" style="height: 41px"></a>-->
                </div>
            </div>

        </div>
    </div>
    <div class="center_column chapter">
        <a class="anchor" id="features"></a>
        <div class="content_container section_base appimage_section row" id="section_first">
            <div class="appimage_placeholder"></div>
            <div class="center">
                <div class="section">
                    <h3>` +
        content.text_features_f1_title +
        `</h3>
                    <p>` +
        content.text_features_f1_desc +
        `</p>
                    <!--<button>Aktion</button>-->
                </div>
            </div>
        </div>
        <div class="content_container section_base section_reversed row">
            <div class="center section_media">
                <img src="./assets/img/vimeet_web_location.png">
            </div>
            <div class="center">
                <div class="section">
                    <h3>` +
        content.text_features_f2_title +
        `</h3>
                    <p>` +
        content.text_features_f2_desc +
        `</p>
                </div>
            </div>
        </div>
        <div class="content_container section_base row">
            <div class="center section_media">
                <img src="./assets/img/vimeet_web_chat.png">
            </div>
            <div class="center">
                <div class="section">
                    <h3>` +
        content.text_features_f3_title +
        `</h3>
                    <p>` +
        content.text_features_f3_desc +
        `</p>
                </div>
            </div>

        </div>
    </div>
    <div class="center_column highlight_base chapter">
        <div class="content_container">
            <div class="about_base">
                <a class="anchor" id="about"></a>
                <h2>` +
        content.text_about_title +
        `</h2>
                <p>` +
        content.text_about_desc +
        `</p>
            </div>

        </div>
    </div>
    <div class="center_column chapter">
        <div class="content_container">
            <a class="anchor" id="development"></a>
            <h2>Neuigkeiten</h2>
            <div id="updates">` +
        updates +
        `</div>
        </div>
    </div>
    <a class="anchor" id="contact"></a>
    <div class="footer_base center_horiz">
        <div class="content_container footer">
            <div class="footer_section">
                <h4>Kontakt</h4>
                <a href="">info (at) vimeet.app</a>
                <!--<a href="#TODO">+49 123 4567</a>-->
            </div>
            <div class="footer_section">
                <h4>Informationen</h4>
                <a href="#download">Download</a>
                <a href="#updates">Entwicklung</a>
                <a href="#TODO">Jobs</a>
            </div>
            <div class="footer_section">
                <h4>Rechtliches</h4>
                <a href="privacy.html">Datenschutz</a>
                <a href="impressum.html">Impressum</a>
            </div>
        </div>
    </div>
    <script>initViMeet();</script> 
</body>
</html>`
    );
}
