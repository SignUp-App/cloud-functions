import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import { getWebsiteContent } from './getWebsiteContent';

export const getWebsite = functions
    .region('europe-west3')
    .https.onRequest((req, res) => {
        admin
            .firestore()
            .collection('website')
            .doc('content')
            .get()
            .then((doc) => {
                if (!doc.exists) {
                    return res.send('Not Found');
                } else {
                    return res.send(getWebsiteContent(doc.data()!));
                }
            })
            .catch((err) => {
                console.log('Error getting document', err);
            });
    });
