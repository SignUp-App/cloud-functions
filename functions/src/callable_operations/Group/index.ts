import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';

import {
    GroupDataWithGroupUserReference,
    GroupDataWithUserReference
} from './interfaces';
import { GroupUserReference } from '../../global_interfaces/UserReference';
import { checkIfUserIsAutheticatedOrThrowError } from '../../util/checkIfAuthenticated';
import { getGroupFromId } from '../../util/getGroup';
import { getUserReferenceFromId } from '../../util/getUser';

//RequestTo Join a Group
export const requestToJoinGroup = functions
    .region('europe-west3')
    .https.onCall(async (groupId: string, context) => {
        checkIfUserIsAutheticatedOrThrowError(context);

        // get Group to check if private
        const group = await getGroupFromId(groupId);

        const currentUser = await getUserReferenceFromId(context.auth!.uid);
        // If group is not private add directly to members
        if (!group.isPrivate) {
            await admin
                .firestore()
                .collection('groups')
                .doc(groupId)
                .update({
                    members: admin.firestore.FieldValue.arrayUnion({
                        ...currentUser,
                        isAdmin: false
                    }),
                    membersReferences: admin.firestore.FieldValue.arrayUnion(
                        context.auth!.uid
                    )
                });
        }
        // Otherwise add to people requesting to join
        else {
            await admin
                .firestore()
                .collection('groups')
                .doc(groupId)
                .update({
                    requestedToJoin:
                        admin.firestore.FieldValue.arrayUnion(currentUser)
                });
        }
    });

//Unsubscribe User from Post
export const leaveGroup = functions
    .region('europe-west3')
    .https.onCall(async (groupId: string, context) => {
        checkIfUserIsAutheticatedOrThrowError(context);

        const currentGroup = await getGroupFromId(groupId);

        const currentGroupUserRef = currentGroup.members.find(
            (user) => user.id === context.auth?.uid
        );
        if (!currentGroupUserRef) {
            throw new functions.https.HttpsError(
                'failed-precondition',
                'User not found in Group'
            );
        }
        await admin
            .firestore()
            .collection('groups')
            .doc(groupId)
            .update({
                members:
                    admin.firestore.FieldValue.arrayRemove(currentGroupUserRef),
                membersReferences: admin.firestore.FieldValue.arrayRemove(
                    currentGroupUserRef.id
                )
            });

        // ToDo delete Group after last person left Group
        return;
    });

//Unsubscribe User from Post
export const abortJoinRequest = functions
    .region('europe-west3')
    .https.onCall(async (groupId: string, context) => {
        checkIfUserIsAutheticatedOrThrowError(context);

        const currentGroup = await getGroupFromId(groupId);

        const currentUserRef = currentGroup.requestedToJoin?.find(
            (user) => user.id === context.auth?.uid
        );

        if (!currentUserRef) {
            throw new functions.https.HttpsError(
                'failed-precondition',
                'User not found in Group'
            );
        }

        await admin
            .firestore()
            .collection('groups')
            .doc(groupId)
            .update({
                requestedToJoin:
                    admin.firestore.FieldValue.arrayRemove(currentUserRef)
            });
        return;
    });

export const acceptUserToGroup = functions
    .region('europe-west3')
    .https.onCall(async (data: GroupDataWithUserReference, context) => {
        checkIfUserIsAutheticatedOrThrowError(context);

        // Check if user has permission to accept to group
        const group = await getGroupFromId(data.groupId);

        const currentUserReferenceFromGroup = group.members.find(
            (user: GroupUserReference) => user.id === context.auth!.uid
        );
        if (
            !currentUserReferenceFromGroup ||
            !currentUserReferenceFromGroup.isAdmin
        ) {
            throw new functions.https.HttpsError(
                'failed-precondition',
                'User does not have permission to admit someone to the group'
            );
        }

        // User has permission accept user
        await admin
            .firestore()
            .collection('groups')
            .doc(data.groupId)
            .update({
                members: admin.firestore.FieldValue.arrayUnion({
                    ...data.user,
                    isAdmin: false
                }),
                membersReferences: admin.firestore.FieldValue.arrayUnion(
                    data.user.id
                ),
                requestedToJoin: admin.firestore.FieldValue.arrayRemove(
                    data.user
                )
            });
    });

export const declineUserFromGroup = functions
    .region('europe-west3')
    .https.onCall(async (data: GroupDataWithUserReference, context) => {
        checkIfUserIsAutheticatedOrThrowError(context);

        const group = await getGroupFromId(data.groupId);

        // Check is user has right to decline
        const currentUserReferenceFromGroup = group.members.find(
            (user: GroupUserReference) => user.id === context.auth!.uid
        );
        if (
            !currentUserReferenceFromGroup ||
            !currentUserReferenceFromGroup.isAdmin
        ) {
            throw new functions.https.HttpsError(
                'failed-precondition',
                'User does not have permission to admit someone to the group'
            );
        }

        await admin
            .firestore()
            .collection('groups')
            .doc(data.groupId)
            .update({
                requestedToJoin: admin.firestore.FieldValue.arrayRemove(
                    data.user
                )
            });
    });

export const removeUserFromGroup = functions
    .region('europe-west3')
    .https.onCall(async (data: GroupDataWithGroupUserReference, context) => {
        checkIfUserIsAutheticatedOrThrowError(context);

        const group = await getGroupFromId(data.groupId);

        // Check is user has right to remove
        const currentUserReferenceFromGroup = group.members.find(
            (user: GroupUserReference) => user.id === context.auth!.uid
        );
        if (
            !currentUserReferenceFromGroup ||
            !currentUserReferenceFromGroup.isAdmin
        ) {
            throw new functions.https.HttpsError(
                'failed-precondition',
                'User does not have permission to remove someone from the group'
            );
        }

        await admin
            .firestore()
            .collection('groups')
            .doc(data.groupId)
            .update({
                members: admin.firestore.FieldValue.arrayRemove(data.user),
                membersReferences: admin.firestore.FieldValue.arrayRemove(
                    data.user.id
                )
            });
    });

export const promoteUserToAdmin = functions
    .region('europe-west3')
    .https.onCall(async (data: GroupDataWithGroupUserReference, context) => {
        checkIfUserIsAutheticatedOrThrowError(context);

        const group = await getGroupFromId(data.groupId);

        // Check is user has right to remove
        const currentUserReferenceFromGroup = group.members.find(
            (user: GroupUserReference) => user.id === context.auth!.uid
        );
        if (
            !currentUserReferenceFromGroup ||
            !currentUserReferenceFromGroup.isAdmin
        ) {
            throw new functions.https.HttpsError(
                'failed-precondition',
                'User does not have permission to promote someone to admin'
            );
        }

        const userToBePromoted = group.members.find(
            (user: GroupUserReference) => user.id === data.user.id
        );

        if (!userToBePromoted) {
            throw new functions.https.HttpsError(
                'failed-precondition',
                'Could not find user which should be made admin'
            );
        }
        userToBePromoted.isAdmin = true;

        await admin
            .firestore()
            .collection('groups')
            .doc(data.groupId)
            .update(group);
    });
