import {
    GroupUserReference,
    UserReference
} from '../../global_interfaces/UserReference';

export interface GroupDataWithUserReference {
    groupId: string;
    user: UserReference;
}

export interface GroupDataWithGroupUserReference {
    groupId: string;
    user: GroupUserReference;
}
