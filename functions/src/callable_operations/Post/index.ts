import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import { checkIfUserIsAutheticatedOrThrowError } from '../../util/checkIfAuthenticated';
import { Post } from '../../global_interfaces/Post';
import { getUserReferenceFromId } from '../../util/getUser';

//Subscribe User to Post
export const subscribeToPost = functions
    .region('europe-west3')
    .https.onCall(async (postId: string, context) => {
        checkIfUserIsAutheticatedOrThrowError(context);

        const currentUser = await getUserReferenceFromId(context.auth!.uid);
        //Load Post from Database
        const post = (
            await admin.firestore().collection('posts').doc(postId).get()
        ).data() as Post | undefined;
        if (!post) {
            throw new functions.https.HttpsError(
                'invalid-argument',
                'No post matching the givenpostId has been found'
            );
        }
        //Check if user is already particpant
        if (
            post.participants.find(
                (participant) => participant.id === currentUser.id
            )
        ) {
            throw new functions.https.HttpsError(
                'invalid-argument',
                'User already Subscribed to post'
            );
        }
        // Check if uscer can Subscribe to Post
        if (
            !post.maxParticipants ||
            post.maxParticipants === -1 ||
            post.participants.length < post.maxParticipants
        ) {
            //Add User to Post
            await admin
                .firestore()
                .collection('posts')
                .doc(postId)
                .update({
                    participants:
                        admin.firestore.FieldValue.arrayUnion(currentUser)
                });
            return;
        } else {
            // Throwing an HttpsError so that the client gets the error details.
            throw new functions.https.HttpsError(
                'invalid-argument',
                'The post has already reached its limit of Subscribers'
            );
        }
    });

//Unsubscribe User from Post
export const unsubscribeFromPost = functions
    .region('europe-west3')
    .https.onCall(async (postId: string, context) => {
        checkIfUserIsAutheticatedOrThrowError(context);

        const currentUser = await getUserReferenceFromId(context.auth!.uid);

        await admin
            .firestore()
            .collection('posts')
            .doc(postId)
            .update({
                participants:
                    admin.firestore.FieldValue.arrayRemove(currentUser)
            });
        return;
    });
