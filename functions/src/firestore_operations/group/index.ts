import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import { Group } from '../../global_interfaces/Group';

export const groupUpdated = functions
    .region('europe-west3')
    .firestore.document('groups/{docId}')
    .onUpdate(async (change, _) => {
        const groupAfter = change.after.data() as Group;
        const firebaseReference = admin
            .firestore()
            .collection('groups')
            .doc(change.after.id);

        // if group is empty delete group
        if (groupAfter.members.length === 0) {
            return firebaseReference.delete();
        }

        // if there is inconsitency between members and mebersReferences
        if (groupAfter.members.length !== groupAfter.membersRefernces?.length) {
            groupAfter.membersRefernces = groupAfter.members.map(
                (member) => member.id
            );
        }

        // if group does not contain ansy admin, make someone admin
        if (!hasAdmin(groupAfter)) {
            groupAfter.members = groupAfter.members.map((member) => {
                member.isAdmin = true;
                return member;
            });
        }

        // finally update group
        return firebaseReference.update(groupAfter);
    });

function hasAdmin(group: Group) {
    for (const member of group.members) {
        if (member.isAdmin) {
            return true;
        }
    }
    return false;
}
