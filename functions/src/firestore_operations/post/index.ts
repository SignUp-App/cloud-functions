import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import { Post } from '../../global_interfaces/Post';

export const postCreated = functions
    .region('europe-west3')
    .firestore.document('posts/{docId}')
    .onCreate(async (snapshot, _) => {
        const post = snapshot.data() as Post;

        // eslint-disable-next-line no-void
        void admin
            .firestore()
            .collection('posts')
            .doc(snapshot.id)
            .update({
                searchableTags: createSearchableTags(post.tags)
            });
    });

export const postUpdated = functions
    .region('europe-west3')
    .firestore.document('posts/{docId}')
    .onUpdate(async (change, _) => {
        const post = change.after.data() as Post;

        //ToDo check if really necessary to upate post

        // eslint-disable-next-line no-void
        void admin
            .firestore()
            .collection('posts')
            .doc(change.after.id)
            .update({
                searchableTags: createSearchableTags(post.tags)
            });
    });

function createSearchableTags(tags: string[]) {
    const set = [],
        listSize = tags.length,
        combinationsCount = 1 << listSize;

    for (let i = 1; i < combinationsCount; i++) {
        let combination = '';
        for (let j = 0; j < listSize; j++) {
            if (i & (1 << j)) {
                combination += tags[j] + ',';
            }
        }
        set.push(combination.substring(0, combination.length - 1));
    }
    return set;
}
