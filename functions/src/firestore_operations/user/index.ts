import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import { trasformUserSnaphshotToUserReference } from '../../util/getUser';
import {
    GroupUserReference,
    UserReference
} from '../../global_interfaces/UserReference';
import { Post } from '../../global_interfaces/Post';
import { Group } from '../../global_interfaces/Group';

export const userUpdated = functions
    .region('europe-west3')
    .firestore.document('users/{docId}')
    .onUpdate(async (change, _) => {
        // update posts
        const posts = await admin
            .firestore()
            .collection('posts')
            .where(
                'participants',
                'array-contains',
                trasformUserSnaphshotToUserReference(change.before)
            )
            .get();

        console.log(posts.docs.length);
        posts.forEach((document) => {
            const updatedParticipants = (
                document.data() as Post
            ).participants.map((participant: UserReference) => {
                if (participant.id === change.before.id) {
                    return trasformUserSnaphshotToUserReference(change.after);
                } else {
                    return participant;
                }
            });

            const updatedAuthor =
                (document.data() as Post).author.id ===
                trasformUserSnaphshotToUserReference(change.before).id
                    ? trasformUserSnaphshotToUserReference(change.after)
                    : (document.data() as Post).author;

            // eslint-disable-next-line no-void
            void admin.firestore().collection('posts').doc(document.id).update({
                author: updatedAuthor,
                participants: updatedParticipants
            });
        });

        // update Groups
        const groups = await admin
            .firestore()
            .collection('groups')
            .where('membersReferences', 'array-contains', change.before.id)
            .get();
        groups.forEach((document) => {
            const updatedMembers = (document.data() as Group).members.map(
                (member: GroupUserReference) => {
                    if (member.id === change.before.id) {
                        return {
                            ...trasformUserSnaphshotToUserReference(
                                change.after
                            ),
                            isAdmin: member.isAdmin
                        };
                    } else {
                        return member;
                    }
                }
            );

            // eslint-disable-next-line no-void
            void admin
                .firestore()
                .collection('groups')
                .doc(document.id)
                .update({
                    members: updatedMembers
                });
        });
    });
