import { GroupUserReference, UserReference } from './UserReference';

export interface Group {
    name: string;
    about: string;
    picture?: string;
    isPrivate?: boolean;

    members: GroupUserReference[];
    membersRefernces?: string[];
    requestedToJoin?: UserReference[];
}
