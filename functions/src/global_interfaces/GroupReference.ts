export interface GroupReference {
  id: string;
  name: string;
  picture?: string;
}
