import { UserReference } from './UserReference';

export interface Post {
    title: string;
    author: UserReference;
    createdAt: number;
    expiresAt: number;
    geohash: string;
    participants: UserReference[];
    maxParticipants?: number;
    tags: string[];
    searchableTags?: string[];
}
