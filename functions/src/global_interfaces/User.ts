export interface User {
    name: string;
    savedPosts?: string[];
    picture?: string;
    violationReports?: string[];
}
