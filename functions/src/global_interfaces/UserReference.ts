export interface UserReference {
  id: string;
  name: string;
  picture?: string;
}

export interface GroupUserReference {
  id: string;
  name: string;
  picture?: string;
  isAdmin: boolean;
}
