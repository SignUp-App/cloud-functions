import * as posts from './callable_operations/Post/index';
import * as groups from './callable_operations/Group/index';
import * as website from './Website/index';
import * as firestore_user from './firestore_operations/user/index';
import * as firestore_posts from './firestore_operations/post/index';
import * as firestore_groups from './firestore_operations/group/index';

import * as admin from 'firebase-admin';

admin.initializeApp();

export {
    posts,
    groups,
    website,
    firestore_user,
    firestore_posts,
    firestore_groups
};
