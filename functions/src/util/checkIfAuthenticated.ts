import * as functions from 'firebase-functions';

export function checkIfUserIsAutheticatedOrThrowError(
    context: functions.https.CallableContext
) {
    //Check if User is authenticated
    if (!context.auth) {
        // Throwing an HttpsError so that the client gets the error details.
        throw new functions.https.HttpsError(
            'failed-precondition',
            'The function must be called ' + 'while authenticated.'
        );
    }
}
