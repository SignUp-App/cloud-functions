import * as admin from 'firebase-admin';
import * as functions from 'firebase-functions';
import { Group } from '../global_interfaces/Group';

export async function getGroupFromId(groupId: string): Promise<Group> {
    const groupDoc = await admin
        .firestore()
        .collection('groups')
        .doc(groupId)
        .get();
    if (groupDoc.exists) {
        const groupData = groupDoc.data();
        if (!!groupData) {
            return groupData as Group;
        } else {
            throw new functions.https.HttpsError(
                'failed-precondition',
                'Group Document is empty'
            );
        }
    } else {
        throw new functions.https.HttpsError(
            'failed-precondition',
            'Group could not be found.'
        );
    }
}
