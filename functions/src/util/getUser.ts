import * as admin from 'firebase-admin';
import * as functions from 'firebase-functions';
import { UserReference } from '../global_interfaces/UserReference';

export function trasformUserSnaphshotToUserReference(
    userSnapshot:
        | functions.firestore.QueryDocumentSnapshot
        | FirebaseFirestore.DocumentSnapshot<FirebaseFirestore.DocumentData>
) {
    if (userSnapshot.exists) {
        const userData = userSnapshot.data();
        if (!!userData) {
            if (userData.picture === null) {
                return {
                    name: userData.name,
                    id: userSnapshot.id,
                    picture: userData.picture
                };
            } else {
                return {
                    name: userData.name,
                    id: userSnapshot.id
                };
            }
        } else {
            throw new functions.https.HttpsError(
                'failed-precondition',
                'User Document is empty'
            );
        }
    } else {
        throw new functions.https.HttpsError(
            'failed-precondition',
            'User Document does not exist'
        );
    }
}

export async function getUserReferenceFromId(
    userId: string
): Promise<UserReference> {
    const userDoc = await admin
        .firestore()
        .collection('users')
        .doc(userId)
        .get();
    return trasformUserSnaphshotToUserReference(userDoc);
}
